import sys
from PySide import QtCore, QtGui

from . import resources_rc

app = QtGui.QApplication(sys.argv)
app.setQuitOnLastWindowClosed(False)

defaultIconImage = QtGui.QIcon(':/graphics/trayIcon.png')
recIconImage = QtGui.QIcon(':/graphics/trayIcon-rec.png')

icon = QtGui.QSystemTrayIcon(defaultIconImage)
keyLogger = None
toggleAction = None

def iconActivated(reason):
	if reason != icon.ActivationReason.Context:
		toggle()

def toggle():
	if keyLogger.isEnabled():
		keyLogger.disable()
		icon.setIcon(defaultIconImage)
		toggleAction.setText("Enable")
	else:
		keyLogger.enable()
		icon.setIcon(recIconImage)
		toggleAction.setText("Disable")
	
def selectLogFile():
	global keyLogger
	
	dialogResponse = QtGui.QFileDialog.getSaveFileName(
		None,
		"Select Log File",
		options=0x00000004 # don't confirm overwrite
	)
	if dialogResponse != None and dialogResponse[0] != '':
		keyLogger.setLogFile(dialogResponse[0])
	
def setStatus(status):
	icon.contextMenu().actions()[0].setText("Status : %s" % status)

def start(logger):
	global keyLogger, toggleAction
	
	keyLogger = logger
	menu = QtGui.QMenu()
	
	toggleAction = menu.addAction('Enable')
	toggleAction.triggered.connect(toggle)
	menu.addAction('Set log file').triggered.connect(selectLogFile)
	menu.addAction('Quit').triggered.connect(exit)
	
	icon.setContextMenu(menu)
	icon.activated.connect(iconActivated)
	

	icon.show()
	sys.exit(app.exec_())

def exit():
	if keyLogger.isEnabled():
		keyLogger.disable()
	app.quit()

