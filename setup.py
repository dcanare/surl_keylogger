from distutils.core import setup
import os, sys, time, shutil
import py2exe

shutil.rmtree('dist/', True)

time.sleep(2)
os.mkdir('dist')

sys.argv.append('py2exe')

setup(
    name = 'SURL Key Logger',
    version = time.strftime('%Y-%m-%d'),
    author = 'Dominic Canare',
    author_email = 'dom@greenlighgo.org',
#    windows = [{'script':'keyLogger.py', 'icon_resources': [(1, 'gui/graphics/icon.ico')]}],
    console = ['keyLogger.py'],
    description = 'SURL Key Logger',
    zipfile = 'modules.lib',
    options = {
        'py2exe': {
            'excludes': [
                'email', 'http', 'pydoc_data', 'pywin', 'urllib', 'xml',
                'doctest', 'pdb', 'unittest', 'difflib', 'inspect', 'distutils',
                'pyreadline', 'locale', 'optparse', 'calendar'
            ],
            'includes': [ 'pyHook' ],
            'optimize': 2,
            'compressed': True,
        },
    }
)
