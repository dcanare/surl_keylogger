import pyHook
import time

import gui

class KeyLogger:
	def __init__(self):
		self.keys = [False]*1024
		self.enabled = False
		self.logFile = None
		
	def enable(self):
		if self.logFile == None:
			gui.selectLogFile()
			if self.logFile == None:
				return
		else:
			self.setLogFile(self.logFile.name)
		
		self.enabled = True
			
		self.log("ENABLE")
		
	def disable(self):
		self.log("DISABLE")
		self.enabled = False
		if self.logFile != None:
			self.logFile.close()
		
	def isEnabled(self):
		return self.enabled

	def setLogFile(self, logFileName):
		self.logFile = open(logFileName, "a")

	def log(self, eventType, key="", keyID="", scanCode="", flags=[]):
		timestamp = time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())

		logVars = [timestamp, eventType, key, keyID, scanCode, "+".join(flags)]
		logString = "\t".join(map(str, logVars))
		if self.logFile == None:
			print(logString)
		else:
			print(logString)
			self.logFile.write(logString)
			self.logFile.write("\n")

	def getModifiers(self):
		mods = []
		if self.keys[160] or self.keys[161]:
			mods.append("shift") 
		if self.keys[162] or self.keys[163]:
			mods.append("control")
		if self.keys[164] or self.keys[165]:
			mods.append("alt")
		if self.keys[91]:
			mods.append("super")
			
		return mods

	def keyDown(self, event):
		if not self.enabled:
			return
		
		flags = self.getModifiers()
		if self.keys[event.KeyID]:
			flags.append("repeat")
			
		self.log("DOWN", event.Key, event.KeyID, event.ScanCode, flags)

		self.keys[event.KeyID] = True
		return True
		
	def keyUp(self, event):
		if not self.enabled:
			return
		
		self.log("UP", event.Key, event.KeyID, event.ScanCode, self.getModifiers())
		self.keys[event.KeyID] = False
		return True
		
	def start(self):
		hooker = pyHook.HookManager()
		hooker.KeyDown = self.keyDown
		hooker.KeyUp = self.keyUp
		hooker.HookKeyboard()
		
		gui.start(self)

KeyLogger().start()
